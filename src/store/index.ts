import Vue from 'vue'
import Vuex from 'vuex'
import customers from '@/store/modules/customers'
import dialog from '@/store/modules/dialog'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    customers,
    dialog,
  },
})
