import { CustomersStateType, CustomersType, CustomerType } from '@/types/Customer'

import faker from 'faker'
faker.locale = 'ru'

const state: CustomersStateType = {
  customers: [],
}

const actions = {
  setDefaultCustomersAction(context: any, payload: number) {
    const data = []

    for (let i = 0; i < payload; i++) {
      const bDate = faker.date.between('1977-01-01', '2021-01-05')
      data.push({
        id: i + 1,
        fullName: faker.name.findName(),
        birthDate: bDate.getTime(),
      })
    }
    context.commit('setDefaultCustomersMutation', data)
  },
  deleteCustomersAction(context: any, payload: number[]) {
    context.commit('deleteCustomersMutation', payload)
  },
  addCustomerAction(context: any, payload: CustomerType) {
    context.commit('addCustomerMutation', payload)
  },
  updateCustomerAction(context: any, payload: CustomerType) {
    context.commit('updateCustomerMutation', payload)
  },
}
const mutations = {
  setDefaultCustomersMutation(state: CustomersStateType, payload: CustomersType) {
    state.customers = [...payload]
  },
  deleteCustomersMutation(state: CustomersStateType, payload: number[]) {
    const data = state.customers.filter((item) => !payload.includes(item.id))
    state.customers = [...data]
  },
  addCustomerMutation(state: CustomersStateType, payload: CustomerType) {
    state.customers.push({ ...payload, id: faker.datatype.number() })
  },
  updateCustomerMutation(state: CustomersStateType, payload: CustomerType) {
    state.customers = state.customers.reduce((customers: CustomersType, customer) => {
      const newCustomer = { ...customer }

      if (newCustomer.id === payload.id) {
        newCustomer.fullName = payload.fullName
        newCustomer.birthDate = new Date(payload.birthDate).getTime()
      }

      customers.push(newCustomer)
      return customers
    }, [])
  },
}
const getters = {
  getCustomers() {
    return state.customers
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}
