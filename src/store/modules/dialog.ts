import { DialogActions, DialogStateType, DialogType } from '@/types/Dialog'

const state: DialogStateType = {
  dialog: {
    status: false,
    maxWidth: 290,
    title: '123123',
    description: '123123',
  },
}

const actions = {
  togglePopupAction(context: any, payload: Partial<DialogType>) {
    if (payload?.ids && payload.actionResult === DialogActions.success) {
      context.dispatch('customers/deleteCustomersAction', payload.ids, { root: true })
    }
    context.commit('togglePopupMutation', payload)
  },
}
const mutations = {
  togglePopupMutation(state: DialogStateType, payload: DialogType) {
    state.dialog = { ...payload }
  },
}
const getters = {
  getDialog() {
    return state.dialog
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}
