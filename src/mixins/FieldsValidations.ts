import { Vue, Component } from 'vue-property-decorator'
import moment, { Moment } from 'moment'

type DateTimeConditionFunction = (momentDate: Moment) => boolean
export type ValidationFunction = (val: string) => boolean | string

/**
 * Валидация на максимально заданную дату/время
 * @param val - значение
 * @param message - сообщение в случае если валидация не проходит
 * @param format - формат даты/времени
 * @param strict - флаг строгого соответствия формату
 * @param callback - функция проверки валидности
 */
const isValidDateTime = (
  val: string,
  message: string,
  format: string,
  strict = true,
  callback: DateTimeConditionFunction = () => true,
) => {
  if (!val) {
    return true
  }

  if (val.includes('-')) {
    const range: string[] = val.split('-').map((range) => range.trim())

    const isValid: boolean = range.every((date) => {
      const momentDate: Moment = moment(date, format, strict)

      return momentDate.isValid() && callback(momentDate)
    })

    return !isValid && message ? message : isValid
  } else {
    const momentDate: Moment = moment(val, format, strict)

    const isValid: boolean = momentDate.isValid() && callback(momentDate)

    return !isValid && message ? message : isValid
  }
}

/**
 * Миксин, добавляющий простые функции валидации значений
 */
@Component
export default class FieldsValidations extends Vue {
  /**
   * Валидация на минимальную длину строки
   * @param minLength - минимальная длина
   * @param message   - сообщение в случае если валидация не проходит
   * @returns функция принимающая значение и возвращающая результат валидации
   * - строка или false в случае ошибки
   */
  validateMinLength(minLength = 1, message = ''): ValidationFunction {
    return (val: string): boolean | string => {
      const isValid = !!val && val.length > minLength
      return !isValid && message ? message : isValid
    }
  }

  /**
   * Валидация на наличие значения
   * @param message   - сообщение в случае если валидация не проходит
   * @returns функция принимающая значение и возвращающая результат валидации
   * - строка или false в случае ошибки
   */
  validateRequired(message = ''): ValidationFunction {
    return (val) => ((!val || !val.trim()) && message ? message : !!val)
  }

  /**
   * Валидация на наличие значения, если нужно получать разные типы данных
   * @param message - сообщение в случае если валидация не проходит
   * @returns функция принимающая значение и возвращающая результат валидации
   * - строка или false в случае ошибки
   */
  validateRequiredCustom(message = ''): ValidationFunction {
    return (val) => (!val && message ? message : !!val)
  }

  /**
   * Валидация на целое число
   * @param message   - сообщение в случае если валидация не проходит
   * @returns функция принимающая значение и возвращающая результат валидации
   * - строка или false в случае ошибки
   */
  validateIsNumber(message = ''): ValidationFunction {
    return (val: string) => {
      const re = /[0-9]+/gi
      const isValid = re.test(val)
      return !isValid && message ? message : isValid
    }
  }

  /**
   * Проверка формата строки по регулярному выражению
   * [не указывать флаг g регулярном выражении]
   * @param regexp  - регулярное выражение для проверки строки
   * @param message - сообщение в случае ошибки
   */
  validateStringFormat(regexp: RegExp, message = ''): ValidationFunction {
    return (val: string) => {
      const isValid = regexp.test(val)
      return !isValid && message ? message : isValid
    }
  }

  /**
   * Валидация на целое или дробное число с дополнительными условиями
   * @param min - минимальное значение числа
   * @param max - максимальное значение числа
   * @returns функция принимающая значение и возвращающая результат валидации
   * - строка в случае ошибки
   */
  validateIsNumberEx(min = -Infinity, max = Infinity): ValidationFunction {
    return (val: string) => {
      const valNumber = Number(val)
      const isNumber = !isNaN(valNumber)
      const isValid = isNumber && valNumber >= min && valNumber <= max
      const minMessage = min !== -Infinity ? ` от ${min}` : ''
      const maxMessage = max !== Infinity ? ` до ${max}` : ''
      const message = `Введите числовое значение${minMessage}${maxMessage}`
      return !isValid ? message : isValid
    }
  }

  /**
   * Валидация на соответствие дате/времени в заданном формате
   * @param message - сообщение в случае если валидация не проходит
   * @param format - формат даты/времени
   * @param strict - флаг строгого соответствия формату
   */
  validateIsDateTime(message: string, format: string, strict = true): ValidationFunction {
    return (val: string) => isValidDateTime(val, message, format, strict)
  }

  /**
   * Валидация на соответствие дате/времени в заданном формате
   * @param message - сообщение в случае если валидация не проходит
   * @param format - формат даты/времени
   * @param strict - флаг строгого соответствия формату
   * @param minDate - минимальная дата
   */
  validateIsMinDateTime(
    message: string,
    format: string,
    strict = true,
    minDate?: string,
  ): ValidationFunction {
    const MAX_YEARS_QUANTITY = 100

    const momentMinDate: Moment = minDate
      ? moment(minDate)
      : moment().add(-MAX_YEARS_QUANTITY, 'years').startOf('year')

    return (val: string) => {
      return isValidDateTime(val, message, format, strict, (momentDate) => {
        return momentDate >= momentMinDate
      })
    }
  }

  /**
   * Валидация на максимально заданную дату/время
   * @param message - сообщение в случае если валидация не проходит
   * @param format - формат даты/времени
   * @param strict - флаг строгого соответствия формату
   * @param maxDate - максимальная дата
   */
  validateIsMaxDateTime(
    message: string,
    format: string,
    strict = true,
    maxDate?: string,
  ): ValidationFunction {
    const MAX_YEARS_QUANTITY = 100

    const momentMaxDate: Moment = maxDate
      ? moment(maxDate)
      : moment().add(MAX_YEARS_QUANTITY, 'years').endOf('year')

    return (val: string) => {
      return isValidDateTime(val, message, format, strict, (momentDate) => {
        return momentDate <= momentMaxDate
      })
    }
  }
}
