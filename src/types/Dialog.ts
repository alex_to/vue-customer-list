export type DialogType = {
  status: boolean
  maxWidth?: number
  title: string
  description: string
  actionResult?: string
  ids?: number[]
  [key: string]: any
}

export type DialogStateType = {
  dialog: DialogType
}

export enum DialogActions {
  cancel = 'cancel',
  success = 'success',
}
