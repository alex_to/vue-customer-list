export type CustomerType = {
  id: number
  fullName: string
  birthDate: number | string
}
export type CustomersType = CustomerType[]
export type CustomersStateType = {
  customers: CustomersType
}
