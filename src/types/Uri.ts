export type UriType = {
  tableParams: string
  filter: string
}
