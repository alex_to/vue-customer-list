/*
 * В этом модуле описаны типы компонентов Vuetify,
 * т.к. компоненты Vuetify могут содержать дополнительные свойства и методы, не включённые в интерфейс
 * Vue, и интерфейс этих компонентов нельзя импортировать из самого Vuetify.
 */

import Vue from 'vue'
import { DataOptions, DataPagination, DataTableCompareFunction, DataTableHeader } from 'vuetify'

// <v-form>
export type VForm = Vue & {
  reset: () => void
  resetValidation: () => void
  validate: () => boolean
}

export type VTextField = Vue & {
  reset: () => void
  resetValidation: () => void
  validate: (showError?: boolean) => boolean
  focus: () => void
  blur: () => void
  valid: boolean
  isFocused: boolean
  readonly: boolean
}

export type VFormFieldRule = (v: any) => true | string

export type VFormFieldRules = VFormFieldRule[]

export type VTreeviewNodeScopedProps<T> = {
  item: T
  leaf: boolean
  selected: boolean
  indeterminate: boolean
  active: boolean
  open: boolean
}

export type VMenu = Vue & {
  isActive: boolean
}

export type VSelect = VTextField & {
  isResetting: boolean
}

export type CustomSort = <T extends any = any>(
  items: T[],
  sortBy: string[],
  sortDesc: boolean[],
  locale: string,
  customSorters?: Record<string, DataTableCompareFunction<T>>,
) => T[]

export interface VDataTableProps {
  customSort?: CustomSort
  dark?: boolean
  dense?: boolean
  disableFiltering?: boolean
  disablePagination?: boolean
  disableSort?: boolean
  height?: number | string
  headers?: DataTableHeader[]
  hideDefaultHeader?: boolean
  hideDefaultFooter?: boolean
  itemKey?: string
  items?: any[]
  itemsPerPage?: number
  light?: boolean
  loadingHeight?: number | string
  loading?: boolean | string
  loadingText?: string
  noDataText?: string
  noResultsText?: string
  options?: DataOptions
  page?: number
  serverItemsLength?: number
  sortBy?: string | string[]
  sortDesc?: boolean | boolean[]
}

export interface VDataFooterProps {
  options: DataOptions
  pagination: DataPagination
  itemsPerPageText?: string
}
