import moment from 'moment'
import { UriType } from '@/types/Uri'
import { DataOptions } from 'vuetify'
import { CustomersType } from '@/types/Customer'

export const formatDate = (timestamp: number | string): string => {
  if (!timestamp) return ''

  const date = new Date(timestamp)

  return date.toLocaleDateString()
}

export const unFormatDate = (val: string): string => {
  const isValidDate = moment(val, 'DD.MM.YYYY', true).isValid()

  if (isValidDate) {
    const [day, month, year] = val.split('.')
    return `${year}-${month}-${day}`
  }
  return ''
}

/** пример ссылки для тестов
 http://test.app/?tableParams=sort:-fullName|page:1|perPage=5|selection=1,2&filter=fullName:ова|birthDate:12.12.2012
 **/
export const getQueryObj = (val: Partial<UriType>) => {
  const data: any = {}

  Object.entries(val).forEach(([prop, value]) => {
    data[prop] = value.split('|').reduce((res, item) => {
      const [propName, ...items] = item.split(/[:,=]/g)

      const newItem: any = {}

      newItem[propName] = items.length > 1 ? items.filter((item) => item !== '') : items[0]

      res = { ...res, ...newItem }
      return res
    }, {})
  })

  return data
}

export const buildQueryObj = (
  tableOptions: Partial<DataOptions>,
  selection: CustomersType,
  date: string,
  fioSearch: string,
): Partial<UriType> => {
  const tableParams: string[] = []
  const filter: string[] = []
  const resData: Partial<UriType> = {}

  if (tableOptions.page) {
    tableParams.push(`page:${tableOptions.page}`)
  }

  if (tableOptions.itemsPerPage) {
    tableParams.push(`perPage=${tableOptions.itemsPerPage}`)
  }

  if (selection.length > 0) {
    const selectionString: string = selection
      .reduce((res: number[], item) => {
        res.push(item.id)
        return res
      }, [])
      .join(',')
    tableParams.push(`selection=${selectionString}`)
  }

  if (tableOptions.sortBy?.length) {
    const sort: string[] = tableOptions.sortBy.reduce((res: string[], item, index) => {
      const sortDesc = tableOptions.sortDesc || []
      const newItem = `${sortDesc[index] ? '-' : ''}${item}`

      res.push(newItem)

      return res
    }, [])

    if (sort.length) {
      tableParams.push(`sort=${sort.join(',')}`)
    }
  }

  if (date) {
    filter.push(`birthDate:${formatDate(date)}`)
  }

  if (fioSearch) {
    filter.push(`fullName:${fioSearch}`)
  }

  if (tableParams.length) {
    resData.tableParams = tableParams.join('|')
  }

  if (filter.length) {
    resData.filter = filter.join('|')
  }
  return resData
}

export const declOfNum = (number: number, words: string[]) => {
  return words[
    number % 100 > 4 && number % 100 < 20
      ? 2
      : [2, 0, 1, 1, 1, 2][number % 10 < 5 ? Math.abs(number) % 10 : 5]
  ]
}
