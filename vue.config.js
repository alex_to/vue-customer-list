module.exports = {
  runtimeCompiler: true,

  css: {
    sourceMap: true,
  },

  pwa: {
    manifestCrossorigin: 'anonymous',
  },

  transpileDependencies: ['vuetify'],
}
