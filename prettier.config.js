module.exports = {
  singleQuote: true,
  bracketLine: true,
  bracketSpacing: true,
  printWidth: 100,
  useTabs: false,
  tabWidth: 2,
  trailingComma: 'all',
  semi: false,
  arrowParens: 'always',
}
